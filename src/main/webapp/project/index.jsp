<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>

<!DOCTYPE html>
<%@ include file="../common/comm_css.jsp"%>
<html>   

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">

		<title>接口文档管理系统</title>

		<meta name="keywords" content="">
		<meta name="description" content="">

		<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

		<style type="text/css">
			html {
				margin-right: -6px;
			}
		</style>
	</head>

	<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
		<div id="wrapper">
			<!--左侧导航开始-->
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="nav-close"><i class="fa fa-times-circle"></i>
				</div>
				<div class="sidebar-collapse">
					<ul class="nav" id="side-menu">
						<li class="nav-header">
							<div class="dropdown profile-element">
								<a data-toggle="dropdown" class="dropdown-toggle" href="#">
									<span class="clear">
                                    <span class="block m-t-xs" style="font-size:20px;">
                                        <strong class="font-bold">接口文档管理系统</strong>
                                    </span>
									</span>
								</a>
							</div>
							<div class="logo-element">文档</div>
						</li>
						<li>
							<a class="J_menuItem" href="prolist.jsp">
								<i class="fa fa-suitcase"></i>
								<span class="nav-label">项目管理</span>
							</a>
						</li>
						<li id="user" style="display: none;">
							<a class="J_menuItem" href="user.jsp">
								<i class="fa fa-user"></i>
								<span class="nav-label">用户管理</span>
							</a>
						</li>
						<li class="line dk"></li>
					</ul>
				</div>
			</nav>
			<!--左侧导航结束-->
			<!--右侧部分开始-->
			<div id="page-wrapper" class="gray-bg dashbard-1">
				<div class="row border-bottom">
	                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
	                    <div class="navbar-header">
	                    	<a class="navbar-minimalize minimalize-styl-2 btn btn-info " href="#"><i class="fa fa-bars"></i> </a>
	                    	<span class="navbar-minimalize minimalize-styl-2"><strong>接口文档管系统</strong></span>
	                    </div>
	                    <ul class="nav navbar-top-links navbar-right">
	                    	<li style="vertical-align: middle;">
	                    		<img alt="image" style="width:40px;" class="img-circle img-responsive" src="${pageContext.request.contextPath}/img/a1.png">
	                    	</li>
							<li class="dropdown">
							
								<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
									
									<%=real_name %>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i>
								</a>
								<ul class="dropdown-menu dropdown-alerts" style="width: 150px;"	>
									<li>
										<a data-toggle="modal" data-target="#myModal4">
											<div>
												<i class="fa fa-user fa-fw"></i>&nbsp;&nbsp;修改密码
											</div>
										</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="../loginout.jsp">
											<div>
												<i class="fa fa-circle-o-notch fa-fw"></i>&nbsp;&nbsp;退出系统
											</div>
										</a>
									</li>
								</ul>
							</li>
						</ul>
	                </nav>
	            </div>
				<div class="row J_mainContent" id="content-main">
					<iframe id="J_iframe" width="100%" height="100%" src="prolist.jsp" frameborder="0" data-id="prolist.jsp" seamless></iframe>
				</div>
			</div>
			<!--右侧部分结束-->
		</div>
		<div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">修改密码</h4>
                    </div>
                    <div class="modal-body form-horizontal" style="padding:10px;">
						<div class="form-group">
							<label class="col-sm-2 control-label">原密码</label>

							<div class="col-sm-6">
								<input type="text" class="form-control" id="oldPwd">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">新密码</label>

							<div class="col-sm-6">
								<input type="text" class="form-control" id="newPwd">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">确认密码</label>

							<div class="col-sm-6">
								<input type="text" class="form-control" id="newPwd1">
							</div>
						</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                        <button type="button" class="btn btn-primary" id="sub" data-dismiss="modal">保存</button>
                    </div>
                </div>
            </div>
        </div>
	</body>
	
</html>
<script type="text/javascript">
$(document).ready(function(){
	var role = <%=role %>;
	if(role==2){
		$("#user").show();
	}
});

//确定
$("#sub").click(function(){
	var xco = new XCO();
	
	var oldPwd = $("#oldPwd").val();
	if(oldPwd){
		xco.setStringValue("oldPwd",oldPwd);
	}else{
		layer.msg("请输入原密码", {time: times, icon:no});
		return;
	}
	
	var newPwd = $("#newPwd").val();
	if(newPwd){
		xco.setStringValue("password",newPwd);
	}else{
		layer.msg("请输入新密码", {time: times, icon:no});
		return;
	}
	var newPwd1 = $("#newPwd1").val();
	if(newPwd1){
		if(newPwd1!=newPwd){
			layer.msg("两次密码不一致", {time: times, icon:no});
			return;
		}
	}else{
		layer.msg("请输入确认密码", {time: times, icon:no});
		return;
	}
	
	
	var options ={
		url:"/updateSysPwd.xco",
		data:xco,
		success: function(result){
			if (result.getCode() != 0) {
				layer.msg(result.getMessage(), {time: times, icon:no});
			} else {
				layer.msg("修改成功！", {time: times, icon:ok});
				location.href="../loginout.jsp";
			}
		}
	};
	layer.confirm('是否修改该账户密码？', {
		title:'提示',
	  	btn: ['是的','不要'] //按钮
	}, function(){
		$.doXcoRequest(options);
	})
})
</script>