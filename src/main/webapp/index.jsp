<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	isELIgnored="false"%>
<%
	String key = com.gatherlife.manager.utils.JspUtils
			.getImageVerifyKey();
%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>接口文档管理系统</title>
<meta name="keywords" content="">
<meta name="description" content="">

<link rel="shortcut icon" href="favicon.ico">
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/font-awesome.css?v=4.4.0" rel="stylesheet">

<link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/style.css?v=4.1.0" rel="stylesheet">
<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<script>if(window.top !== window.self){ window.top.location = window.location;}</script>
</head>

<body class="gray-bg">

	<div class="middle-box text-center loginscreen  animated fadeInDown">
		<div>
			<div>
				<h1 class="logo-name">&nbsp;</h1>
				<h2>接口文档管理系统</h2>
				<br>
			</div>

			<form>
				<div class="form-group">
					<input id="username" type="text" class="form-control" placeholder="用户名"
						required="">
				</div>
				<div class="form-group">
					<input id="password" type="password" class="form-control" placeholder="密码"
						required="">
				</div>
				<div class="form-group">
					<input id="verifyCode" type="text" class="form-control" placeholder="验证码" style="float:left;width:65%;margin-top: 0px;" />
					<img style="float:right;width:80px; height:30px;" src="${pageContext.request.contextPath}/jcaptcha/verify.do?key=<%=key %>" onclick="verifyFun()" id="imgVer" />
				</div>
				<div style="clear:both;"></div>
				
				<button type="button" class="btn btn-primary block full-width m-b m-t" onclick="doLogin();">登 录</button>
			</form>
		</div>
	</div>
	<div id="footer" style="width:100%;text-align: right;bottom:10px;right:20px;position: absolute;">
		   <p class=""><span><a target="_blank" style="font-size: 24px;" href="http://www.gatherlife.cn" ><img src="http://www.gatherlife.cn/images/logo.png"/></a></span></p>
	</div>
</body>
</html>
<!-- 全局js -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/js/jquery.min.js?v=2.1.4"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/xco.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-xco-src.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/public.js"></script>
<script src="${pageContext.request.contextPath}/js/layer.js"></script>
<script type="text/javascript">
function doLogin(){
	var xco=new XCO();
    var username=$("#username").val();
    var password=$("#password").val();
    var verifyCode=$("#verifyCode").val();
 	if(username){
 		xco.setStringValue("user_name",$("#username").val());
	}else{
		layer.msg("请输入账号", {time: times, icon:no});
	    $("#username").focus();
	    return;
	}
 	
	if(password){
		xco.setStringValue("password",$("#password").val());
	}else {
		layer.msg("请输入密码", {time: times, icon:no});
	    $("#password").focus();
		return;
	}
	
	if(verifyCode){
		xco.setStringValue("verifyKey","<%=key%>");
		xco.setStringValue("verifyCode",$("#verifyCode").val());
	}else{
		layer.msg("请输入验证码", {time: times, icon:no});
	    $("#verifyCode").focus();
	    return;
	}
	
	var options ={
		url:"/userlogin.xco",
		data:xco,
		success: manage
	};
	$.doXcoRequest(options);
}	

function manage(xco){
	if(xco.getCode()!= 0){
		layer.msg(xco.getMessage(), {time: times, icon:no});	
	}else{
		window;location.href="../project";
	}
}
function verifyFun(){
	var imgVer = document.getElementById("imgVer");
	imgVer.src = '<%=request.getContextPath()%>/jcaptcha/verify.do?key=<%=key%>&time ='+ new Date();
}
//回车键事件
$(function() {
	$(document).keydown(function(event) {
		if (event.keyCode == 13) {
			doLogin();
		}
	})
});
</script>